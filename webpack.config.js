use: "strict";

const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {

    plugins: [
        new MiniCssExtractPlugin({
          // ファイルの出力設定
          filename: "[name].css",
          chunkFilename: "[id].css" // TODO: これはわからん
        })
    ],
    // モード値を production に設定すると最適化された状態で、
    // development に設定するとソースマップ有効でJSファイルが出力される
    mode: 'development',

    // メインとなるJavaScriptファイル（エントリーポイント）
    entry: './app/src/index.js',

    // ファイルの出力設定
    output: {
        // 出力ファイルのディレクトリ名
        path: path.join(__dirname, '/app/public/js'),
        // 出力ファイル名
        filename: 'main.js'
    },

    module: {
        rules: [
            {
                test: /\.txt$/,
                use: 'raw-loader'
            },
            {
                // sass、scssを対象とする
                test: /\.s?[ac]ss$/,
                use: [
                    // 渡されたCSSの内容をStyleタグの形に変換して、さらにそれをHTML上にInjectするJSコードとしっしょにBundleに渡す
                    // スタイルシートをJSからlinkタグに展開する機能
                    // HTML上で読み込めるものにする
                    'style-loader',
                    // CSSを、javascriptのコードとして扱わせる
                    // CSSをバンドルするための機能
                    'css-loader',
                    'sass-loader',

                ]

            },
        ]

    }
};