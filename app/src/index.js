"use strict";

// Bootstrapのスタイルシート側の機能を読み込む
import "bootstrap/scss/bootstrap.scss";

// BootstrapのJavaScript側の機能を読み込む
import 'bootstrap';


import $ from 'jquery';
import txtContainer from './txt-container.js';

window.addEventListener('load', () => {

	const tc = document.getElementsByClassName('js-txt-container')[0];

	tc.appendChild(txtContainer());
});


$(() => {
	$('.big').css('font-size', '3rem');
	$('.small').css('font-size', '0.5rem');
});